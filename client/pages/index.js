import styles from "../styles/Home.module.css";
import Header from "../components/Header";
import StakeDetails from "../components/StakeDetails";
import StakeForm from "../components/StakeForm";
import WithdrawForm from "../components/WithdrawForm";

export default function Home() {
  return (
    <main className="bg-gradient-to-r from-blue-200 to-green-500">
      <div className={` ${styles.container}`}>
        <Header />
        <StakeDetails />
        <div className="flex flex-col">
          <StakeForm />
          <WithdrawForm />
        </div>
      </div>
    </main>
  );
}