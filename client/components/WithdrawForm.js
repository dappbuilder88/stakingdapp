import React from 'react';
import { useWeb3Contract } from 'react-moralis';
import StakingAbi from '../constants/Staking.json';
import TokenAbi from '../constants/RewardToken.json';
import { Form } from 'web3uikit';
import { ethers } from 'ethers';

function WithdrawForm() {
  const stakingAddress = "0x2043653E840F310c75B389546A5569AD67257aB7";
  const tesTokenAddress = "0xBb0d4788d2d7AC74C493F31a9cf2641c44a309c4";

  const { runContractFunction } = useWeb3Contract();

  let approveOptions = {
    abi: TokenAbi.abi,
    contractAddress: tesTokenAddress,
    functionName: 'approve'
  };

  let withdrawOptions = {
    abi: StakingAbi.abi,
    contractAddress: stakingAddress,
    functionName: 'withdraw'
  };

  async function handleWithdrawSubmit(data) {
    const amountToWithdraw = data.data[0].inputResult;
    approveOptions.params = {
      amount: ethers.utils.parseEther(amountToWithdraw, 'ether'),
      spender: stakingAddress
    };

    const tx = await runContractFunction({
      params: approveOptions,
      onError: (error) => console.log(error),
      onSuccess: () => {
        handleApproveSuccess(approveOptions.params.amount);
      }
    });
  }

  async function handleApproveSuccess(amountToStakeFormatted) {
    withdrawOptions.params = {
      amount: amountToStakeFormatted
    };

    const tx = await runContractFunction({
      params: withdrawOptions,
      onError: (error) => console.log(error)
    });

    // await tx.wait(0);
    console.log('Withdraw transaction complete');
  }

  return (
    <div className='text-black w-[400px] mt-2 lg:mt-0'>
      <Form
        onSubmit={handleWithdrawSubmit}
        data={[
          {
            inputWidth: '50%',
            name: 'Amount to withdraw ',
            type: 'number',
            value: '',
            key: 'amountToWithdraw'
          }
        ]}
        title="Withdraw Now!"
      ></Form>
    </div>
  );
}

export default WithdrawForm;