# Staking Dapp with Interactive UI/UX

Below features have been added to the frontend:

1. Earned balance amount
2. Claim reward
3. Withdrawal

## Frontend
![fontend-large-image](./images/app_lg.png "Frontend Large")
![frontend-small-image](./images/app_sm.png "Frontend Small")

## Claim Reward
![claim-reward-image-1](./images/claim_reward.png "Claim Reward 1")
![claim-reward-image-2](./images/claim_reward_2.png "Claim Reward 2")

## Withdraw
![withdraw-image](./images/withdraw.png "Withdraw")

## Polygon scan
![polygonscan-image](./images/polygonscan.png "Polygonscan")